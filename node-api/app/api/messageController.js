var api = {}

const QUEUE_URL = 'https://sqs.sa-east-1.amazonaws.com/417796969030/';

const AWS = require('aws-sdk')
AWS.config.update({
    region: 'sa-east-1'
})
const sqs = new AWS.SQS();

api.sqsReceiveMessage = function(req, res) {

    const model = req.body;

    (async () => {

        const messages = await sqs.receiveMessage({
            QueueUrl: QUEUE_URL + model.QueueName,
            MaxNumberOfMessages: 1,
            WaitTimeSeconds: 0
        }).promise();

        return res.status(200).json({ result: messages });

    })();
};

api.sqsSendMessage = function(req, res) {
    
    const model = req.body;

    (async () => {
        await sqs.sendMessage({
            MessageBody: model.Mensagem,
            QueueUrl: QUEUE_URL + model.QueueName
        }).promise();
        
        response = 'mensagem enviada com sucesso!';
        return res.status(200).json({ result: response });

    })();
};

api.sqsSendFifoMessage = function(req, res) {
    
    const model = req.body;

    (async () => {
        await sqs.sendMessage({
            MessageBody: model.Mensagem,
            QueueUrl: QUEUE_URL + model.QueueName,
            MessageGroupId: model.MessageGroupId,
            MessageDeduplicationId: model.MessageDeduplicationId
        }).promise();
        
        response = 'mensagem enviada com sucesso!';
        return res.status(200).json({ result: response });

    })();
};

api.sqsDeleteMessage = function(req, res) {
    
    const model = req.body;

    (async () => {
        await sqs.deleteMessage({
            QueueUrl: QUEUE_URL + model.QueueName,
            ReceiptHandle: model.ReceiptHandle
        }).promise();
        
        response = 'mensagem excluída com sucesso!';
        return res.status(200).json({ result: response });

    })();
};

module.exports = api;