/* Código simplório, apenas para fornecer o serviço para a aplicação */

var api = require('../api/messageController');

module.exports  = function(app) {
        
    app.route('/sqsReceiveMessage')
        .post(api.sqsReceiveMessage);
    
    app.route('/sqsSendMessage')
        .post(api.sqsSendMessage);

    app.route('/sqsSendFifoMessage')
        .post(api.sqsSendFifoMessage);        

    app.route('/sqsDeleteMessage')
        .post(api.sqsDeleteMessage);        
};