################################## CREATE ##################################
aws cloudformation create-stack `
--profile default --region us-east-1 `
--stack-name hands-on-sqs-sns-stack `
--template-body file://infra/models/hands-on-sqs-sns-stack.yml `

aws cloudformation create-stack --profile default --region us-east-1 --stack-name hands-on-sqs-sns-stack --template-body file://infra/models/hands-on-sqs-sns-stack.yml