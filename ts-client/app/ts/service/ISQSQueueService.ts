import { Mensagem } from '../models/index';

export interface ISQSQueueService {

    receiveMessage(): void;

    sendMessage(mensagem?: string, messageGroupId?: string, messageDeduplicationId?: string): void;

    deleteMessage(receiptHandle?: string): void;
}