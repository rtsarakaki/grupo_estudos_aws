import { ISQSQueueService } from './ISQSQueueService'

export class SQSQueueStandardService implements ISQSQueueService {

    receiveMessage(): any {

        return fetch(
            'http://localhost:8080/sqsReceiveMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'QueueName': 'HandsOnQueueStandard'
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível importar as negocições.')
        })        
    }

    sendMessage(mensagem?: string, messageGroupId?: string, messageDeduplicationId?: string) {

        return fetch(
            'http://localhost:8080/sqsSendMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'Mensagem': mensagem,
                        'QueueName': 'HandsOnQueueStandard'
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível enviar mensagem.')
        })        
    }

    deleteMessage(receiptHandle?: string) {

        return fetch(
            'http://localhost:8080/sqsDeleteMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'ReceiptHandle': receiptHandle,
                        'QueueName': 'HandsOnQueueStandard'
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível enviar mensagem.')
        })        
    }
}