import { ISQSQueueService } from './ISQSQueueService'

export class SQSQueueFIFOService implements ISQSQueueService {

    receiveMessage(): any {

        return fetch(
            'http://localhost:8080/sqsReceiveMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'QueueName': 'HandsOnQueue.fifo'                        
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível importar as negocições.')
        })        
    }

    sendMessage(mensagem?: string, messageGroupId?: string, messageDeduplicationId?: string) {

        return fetch(
            'http://localhost:8080/sqsSendFifoMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'QueueName': 'HandsOnQueue.fifo',
                        'Mensagem': mensagem,                        
                        'MessageGroupId': messageGroupId,
                        'MessageDeduplicationId': messageDeduplicationId
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível enviar mensagem.')
        })        
    }

    deleteMessage(receiptHandle?: string) {

        return fetch(
            'http://localhost:8080/sqsDeleteMessage',
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(
                    {
                        'ReceiptHandle': receiptHandle,
                        'QueueName': 'HandsOnQueue.fifo'
                    }
                )
            }
        )
        .then(res => res.json())
        .catch(err => { 
            console.log(err);
            throw Error('Não foi possível enviar mensagem.')
        })        
    }
}