import { IndexController } from './controllers/IndexController';

const controller = new IndexController();

$('.form').submit(controller.adiciona.bind(controller));
$('#botao-polling').click(controller.controlarPolling.bind(controller));
$('#botao-excluir').click(controller.excluirMensagem.bind(controller));
$('#radio-handson-queue-standard').click(controller.filaStandardConfigurar.bind(controller));
$('#radio-handson-queue-fifo').click(controller.filaFifoConfigurar.bind(controller));

