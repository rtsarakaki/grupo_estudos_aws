import { View } from './index';
import { Mensagem } from '../models/index'
import { domInject } from '../helpers/decorator/index';

export class IndexView extends View<Mensagem | undefined> {

    @domInject('#input-mensagem')
    public txtMensagem: JQuery;

    @domInject('#input-repeticoes')
    public txtRepeticoes: JQuery;

    @domInject('#input-message-group-id')
    public txtMessageGroupId: JQuery;

    @domInject('#input-message-deduplication-id')
    public txtMessageDeduplicationId: JQuery;

    @domInject('#botao-polling')
    public btnPolling: JQuery;

    @domInject('#botao-excluir')
    public btnExcluir: JQuery;

    @domInject('#radio-handson-queue-standard')
    public optHandsOnQueueStandard: JQuery;

    @domInject('#radio-handson-queue-fifo')
    public optHandsOnQueueFifo: JQuery;

    @domInject('#div-message-group-id')
    public divMessageGroupId: JQuery;

    @domInject('#div-queue-type')
    public divQueueType: JQuery;    

    clear() {
        this.txtMensagem.val("");
        this.txtRepeticoes.val("0");
        this.txtMessageGroupId.val("");
        this.txtMessageDeduplicationId.val("");
    }

    ehFilaStandard(): boolean {
        return this.getInputElement('radio-handson-queue-standard').checked;
    }

    filaStandardConfigurar() {
        this.divMessageGroupId.attr({style:"display:none"});
        this.txtMessageGroupId.attr({required:"false"});
        this.txtMessageDeduplicationId.attr({required:"false"});
    }

    filaFifoConfigurar() {
        this.divMessageGroupId.attr({style:"display:block"});
        this.txtMessageGroupId.attr({required:"true"});
        this.txtMessageDeduplicationId.attr({required:"true"});
    }

    botaoPollingMudarStatus(ligar: boolean) {
        if (ligar) {
            this.btnPolling.text("Ligar Polling").attr({title:"Ligar Polling"});
            this.divQueueType.attr({style:"display:block"});            
        }
        else {
            this.btnPolling.text("Desligar Polling").attr({title:"Desligar Polling"});
            this.divQueueType.attr({style:"display:none"});
        }
    }

    botaoExcluirMudarStatus(visivel: boolean){
        if (visivel) {
            this.btnExcluir.attr({style:"visibility:visible"});
        }
        else {
            this.btnExcluir.attr({style:"visibility:hidden"});
        }        
    }

    template(model : Mensagem): string {

        if (model){
            this.botaoExcluirMudarStatus(true);
            return `
                <div>
                    <label>MessageId: ${model.messageId}</label><br>
                    <label>Body: ${model.body}</label><br>
                    <label>Receipt Handle: </label>
                    <code>${model.receiptHandle}</code><br><br>
                    <label>Atualizado em ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}</label><br><br>                
                </div>
            `; 
        }
        else {
            this.botaoExcluirMudarStatus(false);
            return `
                <div>
                    <label>Nenhuma mensagem foi encontrada.</label><br>
                    <label>Atualizado em ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}</label><br><br>                
                </div>
            `;             
        }
    }
}
