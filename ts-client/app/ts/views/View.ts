//classe base para as views
export abstract class View<T> { 
    
    protected _elemento: JQuery;
    private _escapar: boolean;

    constructor(seletor: string, escapar: boolean = false){

        this._elemento = $(seletor);
        this._escapar = escapar;

    }

    getElement(elementId: string): HTMLElement {
        return (<HTMLElement>document.getElementById(elementId));
    }

    getInputElement(elementId: string): HTMLInputElement {
        return (<HTMLInputElement> this.getElement(elementId));
    }

    update (model: T) {

        try {
            let template = this.template(model);

            if (this._escapar) {
                template = template.replace(/<script>[\s\S]*?<\/script>/g, '');
            }
    
            this._elemento.html(template);
            this.clear();           
        }
        catch (_e){
            let e:Error = _e;
        }
        finally{

        }
    }

    abstract clear(): void;

    abstract template(model: T): string;
}
