export function throttle(tempoEmMileSegundos: number = 500) {

    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor){

        const metodoOriginal = descriptor.value;

        descriptor.value = function(...args: any[]){
        
            if(window.event) window.event.preventDefault();
            metodoOriginal.apply(this, args);
        }

        return descriptor;

    }
}