import { Mensagem } from './Mensagem';

export class Mensagens {

    private _mensagens: Mensagem[] = [];

    adiciona (mensagem : Mensagem) {
        this._mensagens.push(mensagem);
    }
    
    toArray(): Mensagem[] {
        return ([] as Mensagem[]).concat(this._mensagens);
    }

}