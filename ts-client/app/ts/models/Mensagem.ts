export class Mensagem {

    private _timestamp: string;

    constructor (public body?: string, public messageId?: string, public receiptHandle?: string) {
        const date = new Date();
        this._timestamp = `${date.getFullYear()}/${date.getMonth()}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    }

    get timestamp (): string {        
        return this._timestamp;
    }    
}