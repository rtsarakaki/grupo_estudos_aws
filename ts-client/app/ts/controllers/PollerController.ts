abstract class PollerBaseController {

	static readonly INTERVAL = {
		SHORT: 10000,
		STANDARD: 20000,
		LONG: 30000,
	};

	protected timeoutId: number | undefined;

	abstract starPolling(invoke: Function, interval: number, callback: Function): void;

	stopPolling() {
		if (this.timeoutId) {
			clearTimeout(this.timeoutId);
			this.timeoutId = undefined;
		}
	}

	get pollingStarted() {
		return this.timeoutId !== undefined;
	}
}

/**
 * Basic Poller: calls the callback with every response. If the callback returns
 * (a promise that resolves to) false the poller is stopped. In all other cases
 * the poller continues.
 */
export class BasicPollerController extends PollerBaseController {

	private callback: (response: any) => any;
	public interval: number = BasicPollerController.INTERVAL.STANDARD;

	starPolling(invoke: Function, interval = BasicPollerController.INTERVAL.STANDARD, callback: (response: any) => any) {
		this.interval = interval;
		this.poll(invoke);
		this.callback = callback;
		this.schedulePoll(invoke);
	}

	private schedulePoll(invoke: Function) {
		this.timeoutId = window.setTimeout(
			async () => {
				if (await this.poll(invoke) !== false) {
					this.schedulePoll(invoke);
				}
			},
			this.interval,
		);
	}

	private async poll(invoke: Function) {
		const response = await invoke();
		return this.callback(response);
	}
}