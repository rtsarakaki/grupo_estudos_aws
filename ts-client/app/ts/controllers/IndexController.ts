import { Mensagem } from '../models/index';
import { IndexView } from '../views/index';
import { MensagemView } from '../views/index';
import { throttle } from '../helpers/decorator/index';
import { BasicPollerController } from './index'
import { SQSQueueStandardService, SQSQueueFIFOService, ISQSQueueService } from '../service/index';

export class IndexController extends BasicPollerController {

    private _mensagemRecebida = new Mensagem();
    private _indexView = new IndexView('#mensagensView', true);
    private _mensagemView = new MensagemView('#mensagemView');
    public QueueName = "HandsOnQueueStandard";
    private _sqsQueueService: ISQSQueueService;    

    @throttle()
    async adiciona () {        

        let messageGroupId: string;
        let messageDeduplicationId: string;
        const mensagem = this._indexView.txtMensagem.val()?.toString();

        this._mensagemView.update('Enviando mensagem ... ');
        
        if (this._indexView.ehFilaStandard()) {
            this._sqsQueueService = new SQSQueueStandardService();
            messageGroupId = '';
            messageDeduplicationId = '';
        }
        else {
            this._sqsQueueService = new SQSQueueFIFOService();
            messageGroupId = (<string> this._indexView.txtMessageGroupId.val());
            messageDeduplicationId = (<string> this._indexView.txtMessageDeduplicationId.val());
        }

        let repeticoes: number;
        repeticoes = (<number> this._indexView.txtRepeticoes.val());

        if (repeticoes > 0) {

            repeticoes++;

            for (let i = 1; i < repeticoes; i++) {

                //se for enviar mais de uma cópia, adiciona prefixo para identificar as cópias
                let prefixo = `[ID ${i}] - `;
                await this._sqsQueueService.sendMessage(prefixo + mensagem, messageGroupId, i + "." + messageDeduplicationId);
                console.log(prefixo + mensagem)
            }
        }
        else {
            await this._sqsQueueService.sendMessage(mensagem, messageGroupId, messageDeduplicationId);
            console.log(mensagem)
        }

        this._mensagemView.update('Mensagem enviada com sucesso!');
    }

    @throttle()
    controlarPolling() {

        console.log("controlarPolling");

        if (this.pollingStarted) {
            this.stopPolling();
            this._indexView.botaoPollingMudarStatus(!this.pollingStarted);
        } 
        else {

            if (this._indexView.ehFilaStandard()) {
                this._sqsQueueService = new SQSQueueStandardService();
            }
            else {
                this._sqsQueueService = new SQSQueueFIFOService();
            }

            console.log(this._sqsQueueService);
            this.starPolling(this._sqsQueueService.receiveMessage, BasicPollerController.INTERVAL.SHORT, this.mensagemRecebida);
            this._indexView.botaoPollingMudarStatus(!this.pollingStarted);
        }
    }

    @throttle()
    async excluirMensagem() {
        this._mensagemView.update('Excluindo mensagem ... ');

        this._indexView.botaoExcluirMudarStatus(false);

        await this._sqsQueueService.deleteMessage(this._mensagemRecebida.receiptHandle);

        this._mensagemView.update('Mensagem excluída com sucesso!');
    }

    @throttle()
    mensagemRecebida(resource: any) {
        if (resource.result.hasOwnProperty('Messages')) {
            const msg = resource.result.Messages[0];
            this._mensagemRecebida = new Mensagem(msg.Body, msg.MessageId, msg.ReceiptHandle);
            this._indexView.update(this._mensagemRecebida);
        }
        else {
            this._indexView.update(undefined);
        }       
    }

    filaStandardConfigurar() {
        this._indexView.filaStandardConfigurar();
    }

    filaFifoConfigurar() {
        this._indexView.filaFifoConfigurar();
    }
}