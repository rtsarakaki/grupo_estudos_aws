System.register(["./controllers/IndexController"], function (exports_1, context_1) {
    "use strict";
    var IndexController_1, controller;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (IndexController_1_1) {
                IndexController_1 = IndexController_1_1;
            }
        ],
        execute: function () {
            controller = new IndexController_1.IndexController();
            $('.form').submit(controller.adiciona.bind(controller));
            $('#botao-polling').click(controller.controlarPolling.bind(controller));
            $('#botao-excluir').click(controller.excluirMensagem.bind(controller));
            $('#radio-handson-queue-standard').click(controller.filaStandardConfigurar.bind(controller));
            $('#radio-handson-queue-fifo').click(controller.filaFifoConfigurar.bind(controller));
        }
    };
});
