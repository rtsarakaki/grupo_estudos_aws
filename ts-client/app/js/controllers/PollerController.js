System.register([], function (exports_1, context_1) {
    "use strict";
    var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    var PollerBaseController, BasicPollerController;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            PollerBaseController = class PollerBaseController {
                stopPolling() {
                    if (this.timeoutId) {
                        clearTimeout(this.timeoutId);
                        this.timeoutId = undefined;
                    }
                }
                get pollingStarted() {
                    return this.timeoutId !== undefined;
                }
            };
            PollerBaseController.INTERVAL = {
                SHORT: 10000,
                STANDARD: 20000,
                LONG: 30000,
            };
            BasicPollerController = class BasicPollerController extends PollerBaseController {
                constructor() {
                    super(...arguments);
                    this.interval = BasicPollerController.INTERVAL.STANDARD;
                }
                starPolling(invoke, interval = BasicPollerController.INTERVAL.STANDARD, callback) {
                    this.interval = interval;
                    this.poll(invoke);
                    this.callback = callback;
                    this.schedulePoll(invoke);
                }
                schedulePoll(invoke) {
                    this.timeoutId = window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
                        if ((yield this.poll(invoke)) !== false) {
                            this.schedulePoll(invoke);
                        }
                    }), this.interval);
                }
                poll(invoke) {
                    return __awaiter(this, void 0, void 0, function* () {
                        const response = yield invoke();
                        return this.callback(response);
                    });
                }
            };
            exports_1("BasicPollerController", BasicPollerController);
        }
    };
});
