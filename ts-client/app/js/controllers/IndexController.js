System.register(["../models/index", "../views/index", "../helpers/decorator/index", "./index", "../service/index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    var index_1, index_2, index_3, index_4, index_5, index_6, IndexController;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
                index_3 = index_2_1;
            },
            function (index_4_1) {
                index_4 = index_4_1;
            },
            function (index_5_1) {
                index_5 = index_5_1;
            },
            function (index_6_1) {
                index_6 = index_6_1;
            }
        ],
        execute: function () {
            IndexController = class IndexController extends index_5.BasicPollerController {
                constructor() {
                    super(...arguments);
                    this._mensagemRecebida = new index_1.Mensagem();
                    this._indexView = new index_2.IndexView('#mensagensView', true);
                    this._mensagemView = new index_3.MensagemView('#mensagemView');
                    this.QueueName = "HandsOnQueueStandard";
                }
                adiciona() {
                    var _a;
                    return __awaiter(this, void 0, void 0, function* () {
                        let messageGroupId;
                        let messageDeduplicationId;
                        const mensagem = (_a = this._indexView.txtMensagem.val()) === null || _a === void 0 ? void 0 : _a.toString();
                        this._mensagemView.update('Enviando mensagem ... ');
                        if (this._indexView.ehFilaStandard()) {
                            this._sqsQueueService = new index_6.SQSQueueStandardService();
                            messageGroupId = '';
                            messageDeduplicationId = '';
                        }
                        else {
                            this._sqsQueueService = new index_6.SQSQueueFIFOService();
                            messageGroupId = this._indexView.txtMessageGroupId.val();
                            messageDeduplicationId = this._indexView.txtMessageDeduplicationId.val();
                        }
                        let repeticoes;
                        repeticoes = this._indexView.txtRepeticoes.val();
                        if (repeticoes > 0) {
                            repeticoes++;
                            for (let i = 1; i < repeticoes; i++) {
                                let prefixo = `[ID ${i}] - `;
                                yield this._sqsQueueService.sendMessage(prefixo + mensagem, messageGroupId, i + "." + messageDeduplicationId);
                                console.log(prefixo + mensagem);
                            }
                        }
                        else {
                            yield this._sqsQueueService.sendMessage(mensagem, messageGroupId, messageDeduplicationId);
                            console.log(mensagem);
                        }
                        this._mensagemView.update('Mensagem enviada com sucesso!');
                    });
                }
                controlarPolling() {
                    console.log("controlarPolling");
                    if (this.pollingStarted) {
                        this.stopPolling();
                        this._indexView.botaoPollingMudarStatus(!this.pollingStarted);
                    }
                    else {
                        if (this._indexView.ehFilaStandard()) {
                            this._sqsQueueService = new index_6.SQSQueueStandardService();
                        }
                        else {
                            this._sqsQueueService = new index_6.SQSQueueFIFOService();
                        }
                        console.log(this._sqsQueueService);
                        this.starPolling(this._sqsQueueService.receiveMessage, index_5.BasicPollerController.INTERVAL.SHORT, this.mensagemRecebida);
                        this._indexView.botaoPollingMudarStatus(!this.pollingStarted);
                    }
                }
                excluirMensagem() {
                    return __awaiter(this, void 0, void 0, function* () {
                        this._mensagemView.update('Excluindo mensagem ... ');
                        this._indexView.botaoExcluirMudarStatus(false);
                        yield this._sqsQueueService.deleteMessage(this._mensagemRecebida.receiptHandle);
                        this._mensagemView.update('Mensagem excluída com sucesso!');
                    });
                }
                mensagemRecebida(resource) {
                    if (resource.result.hasOwnProperty('Messages')) {
                        const msg = resource.result.Messages[0];
                        this._mensagemRecebida = new index_1.Mensagem(msg.Body, msg.MessageId, msg.ReceiptHandle);
                        this._indexView.update(this._mensagemRecebida);
                    }
                    else {
                        this._indexView.update(undefined);
                    }
                }
                filaStandardConfigurar() {
                    this._indexView.filaStandardConfigurar();
                }
                filaFifoConfigurar() {
                    this._indexView.filaFifoConfigurar();
                }
            };
            __decorate([
                index_4.throttle()
            ], IndexController.prototype, "adiciona", null);
            __decorate([
                index_4.throttle()
            ], IndexController.prototype, "controlarPolling", null);
            __decorate([
                index_4.throttle()
            ], IndexController.prototype, "excluirMensagem", null);
            __decorate([
                index_4.throttle()
            ], IndexController.prototype, "mensagemRecebida", null);
            exports_1("IndexController", IndexController);
        }
    };
});
