System.register([], function (exports_1, context_1) {
    "use strict";
    var View;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            View = class View {
                constructor(seletor, escapar = false) {
                    this._elemento = $(seletor);
                    this._escapar = escapar;
                }
                getElement(elementId) {
                    return document.getElementById(elementId);
                }
                getInputElement(elementId) {
                    return this.getElement(elementId);
                }
                update(model) {
                    try {
                        let template = this.template(model);
                        if (this._escapar) {
                            template = template.replace(/<script>[\s\S]*?<\/script>/g, '');
                        }
                        this._elemento.html(template);
                        this.clear();
                    }
                    catch (_e) {
                        let e = _e;
                    }
                    finally {
                    }
                }
            };
            exports_1("View", View);
        }
    };
});
