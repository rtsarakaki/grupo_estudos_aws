System.register(["./index", "../helpers/decorator/index"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var index_1, index_2, IndexView;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (index_1_1) {
                index_1 = index_1_1;
            },
            function (index_2_1) {
                index_2 = index_2_1;
            }
        ],
        execute: function () {
            IndexView = class IndexView extends index_1.View {
                clear() {
                    this.txtMensagem.val("");
                    this.txtRepeticoes.val("0");
                    this.txtMessageGroupId.val("");
                    this.txtMessageDeduplicationId.val("");
                }
                ehFilaStandard() {
                    return this.getInputElement('radio-handson-queue-standard').checked;
                }
                filaStandardConfigurar() {
                    this.divMessageGroupId.attr({ style: "display:none" });
                    this.txtMessageGroupId.attr({ required: "false" });
                    this.txtMessageDeduplicationId.attr({ required: "false" });
                }
                filaFifoConfigurar() {
                    this.divMessageGroupId.attr({ style: "display:block" });
                    this.txtMessageGroupId.attr({ required: "true" });
                    this.txtMessageDeduplicationId.attr({ required: "true" });
                }
                botaoPollingMudarStatus(ligar) {
                    if (ligar) {
                        this.btnPolling.text("Ligar Polling").attr({ title: "Ligar Polling" });
                        this.divQueueType.attr({ style: "display:block" });
                    }
                    else {
                        this.btnPolling.text("Desligar Polling").attr({ title: "Desligar Polling" });
                        this.divQueueType.attr({ style: "display:none" });
                    }
                }
                botaoExcluirMudarStatus(visivel) {
                    if (visivel) {
                        this.btnExcluir.attr({ style: "visibility:visible" });
                    }
                    else {
                        this.btnExcluir.attr({ style: "visibility:hidden" });
                    }
                }
                template(model) {
                    if (model) {
                        this.botaoExcluirMudarStatus(true);
                        return `
                <div>
                    <label>MessageId: ${model.messageId}</label><br>
                    <label>Body: ${model.body}</label><br>
                    <label>Receipt Handle: </label>
                    <code>${model.receiptHandle}</code><br><br>
                    <label>Atualizado em ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}</label><br><br>                
                </div>
            `;
                    }
                    else {
                        this.botaoExcluirMudarStatus(false);
                        return `
                <div>
                    <label>Nenhuma mensagem foi encontrada.</label><br>
                    <label>Atualizado em ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}</label><br><br>                
                </div>
            `;
                    }
                }
            };
            __decorate([
                index_2.domInject('#input-mensagem')
            ], IndexView.prototype, "txtMensagem", void 0);
            __decorate([
                index_2.domInject('#input-repeticoes')
            ], IndexView.prototype, "txtRepeticoes", void 0);
            __decorate([
                index_2.domInject('#input-message-group-id')
            ], IndexView.prototype, "txtMessageGroupId", void 0);
            __decorate([
                index_2.domInject('#input-message-deduplication-id')
            ], IndexView.prototype, "txtMessageDeduplicationId", void 0);
            __decorate([
                index_2.domInject('#botao-polling')
            ], IndexView.prototype, "btnPolling", void 0);
            __decorate([
                index_2.domInject('#botao-excluir')
            ], IndexView.prototype, "btnExcluir", void 0);
            __decorate([
                index_2.domInject('#radio-handson-queue-standard')
            ], IndexView.prototype, "optHandsOnQueueStandard", void 0);
            __decorate([
                index_2.domInject('#radio-handson-queue-fifo')
            ], IndexView.prototype, "optHandsOnQueueFifo", void 0);
            __decorate([
                index_2.domInject('#div-message-group-id')
            ], IndexView.prototype, "divMessageGroupId", void 0);
            __decorate([
                index_2.domInject('#div-queue-type')
            ], IndexView.prototype, "divQueueType", void 0);
            exports_1("IndexView", IndexView);
        }
    };
});
