System.register([], function (exports_1, context_1) {
    "use strict";
    var Mensagem;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            Mensagem = class Mensagem {
                constructor(body, messageId, receiptHandle) {
                    this.body = body;
                    this.messageId = messageId;
                    this.receiptHandle = receiptHandle;
                    const date = new Date();
                    this._timestamp = `${date.getFullYear()}/${date.getMonth()}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
                }
                get timestamp() {
                    return this._timestamp;
                }
            };
            exports_1("Mensagem", Mensagem);
        }
    };
});
