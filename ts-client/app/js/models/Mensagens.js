System.register([], function (exports_1, context_1) {
    "use strict";
    var Mensagens;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            Mensagens = class Mensagens {
                constructor() {
                    this._mensagens = [];
                }
                adiciona(mensagem) {
                    this._mensagens.push(mensagem);
                }
                toArray() {
                    return [].concat(this._mensagens);
                }
            };
            exports_1("Mensagens", Mensagens);
        }
    };
});
