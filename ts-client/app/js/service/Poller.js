System.register([], function (exports_1, context_1) {
    "use strict";
    var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    };
    var PollerBase, BasicPoller;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            PollerBase = class PollerBase {
                stopPolling() {
                    if (this.timeoutId) {
                        clearTimeout(this.timeoutId);
                        this.timeoutId = undefined;
                    }
                }
                get started() {
                    return this.timeoutId !== undefined;
                }
            };
            PollerBase.INTERVAL = {
                SHORT: 15000,
                STANDARD: 30000,
                LONG: 60000,
            };
            BasicPoller = class BasicPoller extends PollerBase {
                constructor() {
                    super(...arguments);
                    this.interval = BasicPoller.INTERVAL.STANDARD;
                }
                starPolling(invoke, interval = BasicPoller.INTERVAL.STANDARD, callback) {
                    this.interval = interval;
                    this.poll(invoke);
                    this.callback = callback;
                    this.schedulePoll(invoke);
                }
                schedulePoll(invoke) {
                    this.timeoutId = window.setTimeout(() => __awaiter(this, void 0, void 0, function* () {
                        if ((yield this.poll(invoke)) !== false) {
                            this.schedulePoll(invoke);
                        }
                    }), this.interval);
                }
                poll(invoke) {
                    return __awaiter(this, void 0, void 0, function* () {
                        const response = yield invoke();
                        return this.callback(response);
                    });
                }
            };
            exports_1("BasicPoller", BasicPoller);
        }
    };
});
