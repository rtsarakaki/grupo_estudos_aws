System.register(["./SQSQueueStandardService", "./SQSQueueFIFOService", "./ISQSQueueService"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function exportStar_1(m) {
        var exports = {};
        for (var n in m) {
            if (n !== "default") exports[n] = m[n];
        }
        exports_1(exports);
    }
    return {
        setters: [
            function (SQSQueueStandardService_1_1) {
                exportStar_1(SQSQueueStandardService_1_1);
            },
            function (SQSQueueFIFOService_1_1) {
                exportStar_1(SQSQueueFIFOService_1_1);
            },
            function (ISQSQueueService_1_1) {
                exportStar_1(ISQSQueueService_1_1);
            }
        ],
        execute: function () {
        }
    };
});
