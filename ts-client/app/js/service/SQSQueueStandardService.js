System.register([], function (exports_1, context_1) {
    "use strict";
    var SQSQueueStandardService;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            SQSQueueStandardService = class SQSQueueStandardService {
                receiveMessage() {
                    return fetch('http://localhost:8080/sqsReceiveMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'QueueName': 'HandsOnQueueStandard'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível importar as negocições.');
                    });
                }
                sendMessage(mensagem, messageGroupId, messageDeduplicationId) {
                    return fetch('http://localhost:8080/sqsSendMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'Mensagem': mensagem,
                            'QueueName': 'HandsOnQueueStandard'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
                deleteMessage(receiptHandle) {
                    return fetch('http://localhost:8080/sqsDeleteMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'ReceiptHandle': receiptHandle,
                            'QueueName': 'HandsOnQueueStandard'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
            };
            exports_1("SQSQueueStandardService", SQSQueueStandardService);
        }
    };
});
