System.register([], function (exports_1, context_1) {
    "use strict";
    var SQSQueueFIFOService;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            SQSQueueFIFOService = class SQSQueueFIFOService {
                receiveMessage() {
                    return fetch('http://localhost:8080/sqsReceiveMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'QueueName': 'HandsOnQueue.fifo'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível importar as negocições.');
                    });
                }
                sendMessage(mensagem, messageGroupId, messageDeduplicationId) {
                    return fetch('http://localhost:8080/sqsSendFifoMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'QueueName': 'HandsOnQueue.fifo',
                            'Mensagem': mensagem,
                            'MessageGroupId': messageGroupId,
                            'MessageDeduplicationId': messageDeduplicationId
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
                deleteMessage(receiptHandle) {
                    return fetch('http://localhost:8080/sqsDeleteMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'ReceiptHandle': receiptHandle,
                            'QueueName': 'HandsOnQueue.fifo'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
            };
            exports_1("SQSQueueFIFOService", SQSQueueFIFOService);
        }
    };
});
