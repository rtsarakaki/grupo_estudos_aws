System.register([], function (exports_1, context_1) {
    "use strict";
    var SQSQueueService;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            SQSQueueService = class SQSQueueService {
                constructor(queueName) {
                    this.QueueName = queueName;
                    console.log("constructor: " + this.QueueName);
                }
                receiveMessage() {
                    console.log("receiveMessage: " + this.QueueName);
                    return fetch('http://localhost:8080/sqsMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'QueueName': 'HandsOnQueueStandard'
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível importar as negocições.');
                    });
                }
                sendMessage(mensagem) {
                    return fetch('http://localhost:8080/sqsMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'Mensagem': mensagem,
                            'QueueName': this.QueueName
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
                deleteMessage(receiptHandle) {
                    return fetch('http://localhost:8080/sqsDeleteMessage', {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({
                            'ReceiptHandle': receiptHandle,
                            'QueueName': this.QueueName
                        })
                    })
                        .then(res => res.json())
                        .catch(err => {
                        console.log(err);
                        throw Error('Não foi possível enviar mensagem.');
                    });
                }
            };
            exports_1("SQSQueueService", SQSQueueService);
        }
    };
});
