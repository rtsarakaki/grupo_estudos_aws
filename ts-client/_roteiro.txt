TS-CLIENT

//instalar typescript
npm install typescript@2.3.2 --save-dev
npm install typescript --save-dev

//instalar jquery
npm install @types/jquery@2.0.42 --save-dev
npm install @types/jquery --save-dev

//instalar servidor lite-server
npm install lite-server@2.3.0 --save-dev
npm install lite-server --save-dev

//instalar concurrently (browsersync)
npm install concurrently@3.4.0 --save-dev
npm install concurrently --save-dev

########################################################################################################

NODE-API
//instalar sdk da aws para poder acessar api dos serviços
npm install aws-sdk --save-dev

PARA CORRIGIR PROBLEMA DE PERMISSÃO PARA EXECUTAR ng NO POWER SHELL
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

